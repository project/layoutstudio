
-- SUMMARY --

Layout Studio is a starting theme that makes it easier for developers to 
implement SEO and accessibility friendly custom design on Drupal sites. 
It includes a CSS reset, beautiful typographical defaults and an easy way 
to change/edit layouts.

For more information, visit the following pages:

  - Project Page: http://drupal.org/project/layoutstudio
  - Project Site: http://www.layoutstudiotheme.com
  - Documentation: TBD
  - Issues: http://drupal.org/project/issues/layoutstudio


-- KEY DIFFERENCES OVER ZEN PROJECT --

While this theme credits the Zen project for inspiration behind some of 
its features, it does differ in significant ways to enhance productivity.

	- Zen uses the "left-sidebar" and "right-sidebar" for its sidebar regions. Layout 
		Studio uses "secondary" and "tertiary", since the secondary or tertiary regions 
		could possibly on either side of the primary content.
	- Layout Studio has more layout options. Zen can only have a right and/or left sidebar, 
		but cannot have two sidebars to the left or the right of the main content area if 
		your layout requires it.
	-	More body, block and node classes classes. 
	- Random class feature.
	- Simpler installation.
	- Easier to setup basic site wireframe.
	- Less complex template/HTML.
	- Built-in "credit link" where you can put your firm's credit text or logo on either the 
		home or all pages.
	-	Built-in function for copyright information and automatic date span (eg. 2008-09).


-- INSTALLATION --

This file is only meant to highlight the key features of the theme. For proper 
installation of the Layout Studio theme can be found in the INSTALL.txt inside 
the WORKING_COPY folder.


-- NOTICE --

* It is strongly recommended that NO CHANGES be made to the Layout Studio base theme.

* Developers should be very comfortable with CSS, HTML and Drupal theming before 
  using this theme. For more information on Drupal Theming, see the Drupal 6 
  Theming Guide - http://drupal.org/theme-guide/6 


-- THEME FEATURES --

  * Available Regions

    In order of HTML/source code output:

      - Header
      - Preface: First, Middle and Last
      - Primary: Pre-Title & Post-Title, Primary (content)
      - Secondary: First, Middle and Last
      - Tertiary: First, Middle and Last
      - Postscript: First, Middle and Last
      - Footer
      - Closure

    Please note that the Primary, Secondary and Tertiary form the basic page 
    content layout.


  * CSS Classes (partial list)

    Body Element Classes/ID

      - Language (ID): "lang-en"
      - Random: this only appears if you set the random field in the theme 
        configuration page to a number higher than 1.
      - Unique Node or View (ie "node-1")
      - Node type
      - Front/Not-Front 
      - Current Menu-item
      - Logged-out/Logged-in
      - Section: first part of the path (ie "/about" creates 
        a "section-about" class)
      -	Sub-Section: second part of the path (ie "/about/mission" 
        creates a "sub-section-mission" class)
      - Indicator for empty secondary, tertiary, preface and postscript 
        regions ("no-secondary" and "no-tertiary")

    Node Template Classes/ID

      - Node ID (ID)
      - Node Author
      - Node Authored by current user ("user-me" class)
      - Teaser or Full Node
      - Odd/Even
      - Node Count
      - Node Type

    Block Template Classes/ID

      - Block ID (ID)
      - Random: this only appears if you set the random field in the theme 
        configuration page to a number higher than 1.
      - Type: for typographic presets
      - Block: common to all blocks
      - Block Module: indicates which module generated the block
      - Odd/Even (within region)
      - Block Count (within region)


  * Theme Configuration Options

    Other than the options available in Drupal core theme configuration page, 
    the following options are available for any sub-themes:

      - Random class: allows for random changes per refresh. Requires a number 
        higher than 1. For example, inserting "5" in the field you randomly create 
        a class between "random-1" to "random-5" on each page refresh.
      - Credit destination link and logo: promotional link for the website 
        development team/company/agency. Options to appear on only the home page,
        all pages or none.
      - Copyright holder and start year: custom ouput of the site's copyright 
        holder and year. Year automatically increases per year (For example, 
        setting "2006" in the copyright start year will display as 2006-2007 
        after one year.)
      - Choose base layout.
      - Colors: ideal for exploring where the regions appear while doing 
        wireframes or when trying to learn how the theme works.

    Please note that you can add additional configuration options in your 
    sub-theme by editing the theme-settings.php file.


-- ACKNOWLEDGEMENTS --

Layout Studio theme for Drupal 6 was partially inspired by:

- Layout Gala from Alessandro Fulciniti: http://blog.html.it/layoutgala/
- Tripoli CSS: http://devkick.com/lab/tripoli/
- Zen theme: http://www.drupal.org/project/zen


-- CONTACT --

Current Maintainers:
* Gord Christmas (codexmas) - http://drupal.org/user/40977
* Rene Hache (rhache) - http://drupal.org/user/64478
* Ryan James (rjay) - http://drupal.org/user/363916

This project was sponsored and maintained by: 

  * northStudio Inc. - http://www.northstudio.com
    northStudio specializes in designing and developing Drupal sites including
    custom web applications, e-commerce and search engine optimization. The Layout
    Studio theme has been used in hundreds of our Drupal sites.
