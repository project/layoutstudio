<?php

/**
 * @file comment.tpl.php
 * Default theme implementation for comments.
 *
 * Available variables:
 * - $author: Comment author. Can be link or plain text.
 * - $content: Body of the post.
 * - $date: Date and time of posting.
 * - $links: Various operational links.
 * - $new: New comment marker.
 * - $picture: Authors picture.
 * - $signature: Authors signature.
 * - $status: Comment status. Possible values are:
 *   comment-unpublished, comment-published or comment-preview.
 * - $submitted: By line with date and time.
 * - $title: Linked title.
 *
 * These two variables are provided for context.
 * - $comment: Full comment object.
 * - $node: Node object the comments are attached to.
 *
 * @see template_preprocess_comment()
 * @see theme_comment()
 */
?>
<div class="comment<?php print ($comment->new) ? ' comment-new' : ''; print ' ' . $status; print ' ' . $comment_classes; ?>">
  <?php print $picture ?>

  <?php if ($comment->new): ?>
    <span class="new"><?php print $new ?></span>
  <?php endif; ?>

  <h3 class="comment-title"><?php print $title ?></h3>

  <div class="submitted">
    <?php print $submitted ?>
  </div><!-- CLASS submitted -->

  <div class="content">

    <?php print $content ?>

    <?php if ($signature): ?>
    <div class="user-signature">
      <?php print $signature ?>
    </div><!-- CLASS user-signature -->
    <?php endif; ?>

  </div><!-- CLASS content -->

  <?php print $links ?>
</div><!-- CLASS comment<?php print ($comment->new) ? ' comment-new' : ''; print ' ' . $status; print ' ' . $comment_classes; ?> -->
