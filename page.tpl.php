<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
	<title><?php print $head_title; ?></title>
	<?php print $head; ?>
	<?php print $styles; ?>
	<?php print $scripts; ?>
</head>

<body id="<?php print 'lang-' . $language->language; ?>" class="<?php print $body_classes;?>">
<p class="skip-to-links">Skip to <a href="#content-area">content</a> - <a href="#navigation">navigation</a></p>
	<div id="page">
		<div id="container">
	
			<div id="header" class="region">
				<?php if ($is_front): ?>
					<?php if ($logo): ?>
						<img title="<?php print $site_name; ?> Home" alt="<?php print $site_name ?> Logo" class="site-logo" src="<? print $logo; ?>" />
					<?php endif; ?>
					<?php if ($site_name): print '<p class="site-name">' . $site_name . '</p>'; endif; ?>
				<?php else: ?>
					<?php if ($logo): ?>
						<a href="<?php print $base_path ?>">
							<img title="<?php print $site_name ?> Home" alt="<?php print $site_name ?> Logo" class="site-logo" src="<? print $logo; ?>" />
						</a> 
					<?php endif; ?>
					<?php if ($site_name): print '<p class="site-name"><a title="' . $site_name . ' Home" href="' . $base_path . '">' . $site_name . '</a></p>'; endif; ?>
				<?php endif; ?>
				<?php if ($site_slogan): print '<p class="site-slogan">' . $site_slogan . '</p>'; endif; ?>
				<?php print $header; ?>
			</div><!-- ID header -->
			
			<?php if ($breadcrumb): print $breadcrumb; endif; ?>
			
			<div id="content">

				<?php if ($preface_first OR $preface OR $preface_last): ?>
					<div id="preface">
						<?php if ($preface_first): ?>
				    	<div class="first region">
				      	<?php print $preface_first; ?>
				      </div><!-- CLASS first region -->
				    <?php endif; ?>
				    <?php if ($preface): ?>
				    	<div class="middle region">
				      	<?php print $preface; ?>
				      </div><!-- CLASS middle region -->
				    <?php endif; ?>
				    <?php if ($preface_last): ?>
				    	<div class="last region">
				      	<?php print $preface_last; ?>
				      </div><!-- CLASS last region -->
				    <?php endif; ?>
					</div><!-- ID preface -->
				<?php endif; ?>
			
				<div id="wrapper">
					<div id="primary">
			      <div class="inside type">
			
			      <?php if ($content_pre_title): ?>
			      	<div id="content_pre_title" class="region">
				      	<?php print $content_pre_title; ?>
				      </div><!-- ID content_pre_title -->
				    <?php endif; ?>
			
						<a id="content-area"></a>
			      <?php if ($tabs): print '<div id="tabs-wrapper">'; endif; ?>
			      <?php if ($title): print '<h1 class="page-title'. ($tabs ? ' with-tabs' : '') . ($content_pre_title ? ' pre-title' : '') . ($content_post_title ? ' post-title' : '') .'">'. $title .'</h1>'; endif; ?>
			      <?php if ($tabs): print $tabs .'</div>'; endif; ?>
			      <?php if (isset($tabs2)): print $tabs2; endif; ?>
			      <?php if ($help): print $help; endif; ?>
			      <?php if ($messages): print $messages; endif; ?>
			
			      <?php if ($content_post_title): ?>
			      	<div id="content-post-title" class="region">
				      	<?php print $content_post_title; ?>
				      </div><!-- ID content_post_title -->
				    <?php endif; ?>
			
			      <?php print $content ?>
						
			      <?php print $feed_icons ?>
			      </div><!-- CLASS primary type -->
					</div><!-- ID primary -->
				</div><!-- ID wrapper -->
				
				<?php if ($secondary_first OR $secondary OR $secondary_last): ?>
					<div id="secondary">
						<?php if ($secondary_first): ?>
				    	<div class="first region">
				      	<?php print $secondary_first; ?>
				      </div><!-- CLASS first region -->
				    <?php endif; ?>
				    <?php if ($secondary): ?>
				    	<div class="middle region">
				      	<?php print $secondary; ?>
				      </div><!-- CLASS middle region -->
				    <?php endif; ?>
				    <?php if ($secondary_last): ?>
				    	<div class="last region">
				      	<?php print $secondary_last; ?>
				      </div><!-- CLASS last region -->
				    <?php endif; ?>
					</div><!-- ID secondary -->
				<?php endif; ?>
			
				<?php if ($tertiary_first OR $tertiary OR $tertiary_last): ?>
					<div id="tertiary">
						<?php if ($tertiary_first): ?>
				    	<div class="first region">
				      	<?php print $tertiary_first; ?>
				      </div><!-- CLASS first region -->
				    <?php endif; ?>
				    <?php if ($tertiary): ?>
				    	<div class="middle region">
				      	<?php print $tertiary; ?>
				      </div><!-- CLASS middle region -->
				    <?php endif; ?>
				    <?php if ($tertiary_last): ?>
				    	<div class="last region">
				      	<?php print $tertiary_last; ?>
				      </div><!-- CLASS last region -->
				    <?php endif; ?>
					</div><!-- ID tertiary -->
				<?php endif; ?>
		
				<?php if ($postscript_first OR $postscript OR $postscript_last): ?>
					<div id="postscript">
						<?php if ($postscript_first): ?>
				    	<div class="first region">
				      	<?php print $postscript_first; ?>
				      </div><!-- CLASS first region -->
				    <?php endif; ?>
				    <?php if ($postscript): ?>
				    	<div class="middle region">
				      	<?php print $postscript; ?>
				      </div><!-- CLASS middle region -->
				    <?php endif; ?>
				    <?php if ($postscript_last): ?>
				    	<div class="last region">
				      	<?php print $postscript_last; ?>
				      </div><!-- CLASS last region -->
				    <?php endif; ?>
					</div><!-- ID postscript -->
				<?php endif; ?>

			</div><!-- ID content -->

		</div> <!-- ID container -->
	
		<div id="footer" class="region">
			<div class="inside">

				<?php if (isset($primary_links)) : ?>
					<div id="primary-nav" class="navigation">
						<a id="navigation"></a>
          	<?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
          </div><!-- ID primary-nav CLASS navigation -->
        <?php endif; ?>

        <?php if (isset($secondary_links)) : ?>
        	<div id="secondary-nav" class="navigation">
          	<?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
          </div><!-- ID secondary-nav CLASS navigation -->
        <?php endif; ?>

				<?php print $footer ?>
				<p class="footer-message"><?php print $footer_message ?></p>
		    <p class="credit-copyright"><?php print $copyright; ?><?php print $credit; ?></p>

			</div><!-- CLASS inside -->
		</div><!-- ID footer -->
		
		<?php if ($closure_region): ?>
    	<div id="closure" class="region">
    		<?php print $closure_region; ?>
    	</div>
  	<?php endif; ?>
  
	</div><!-- ID page -->

	<?php print $closure ?>
	
</body>
</html>
