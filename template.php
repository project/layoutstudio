<?php

if (!is_null(theme_get_setting('layoutstudio_random_class'))) {  
  global $theme_key;
   
  // Get default theme settings.
  $settings = theme_get_settings($theme_key);
  
  // Add CSS stylesheet for selected layout
  drupal_add_css(drupal_get_path("theme", $theme_key)."/layouts/".$settings['layoutstudio_layout'].".css", "theme");
  // Add colors.css if selected
  if($settings['color_options']) {
    drupal_add_css(drupal_get_path("theme", $theme_key)."/layouts/colors.css", "theme");
  }
}

//Setting classes for body element
function layoutstudio_preprocess_page(&$variables) {
	form_clean_id(NULL, TRUE);
	// Set up layout variable.
  $variables['layout'] = 'no-secondary-tertiary';
  if (!empty($variables['secondary_first']) OR !empty($variables['secondary']) OR !empty($variables['secondary_last'])) {
    $variables['layout'] = 'no-tertiary';
  }
  if (!empty($variables['tertiary_first']) OR !empty($variables['tertiary']) OR !empty($variables['tertiary_last'])) {
    $variables['layout'] = ($variables['layout'] == 'no-tertiary') ? 'regions-all' : 'no-secondary';
  }
  $variables['preface_class'] = 'no-preface';
  if (!empty($variables['preface_first']) OR !empty($variables['preface']) OR !empty($variables['preface_last'])) {
    $variables['preface_class'] = 'no-preface';
  }
  $variables['postscript_class'] = 'no-postscript';
  if (!empty($variables['postscript_first']) OR !empty($variables['postscript']) OR !empty($variables['postscript_last'])) {
    $variables['postscript_class'] = 'no-postscript';
  }
	
  // Compile a list of classes that are going to be applied to the body element.
  // This allows advanced theming based on context (home page, node of certain type, etc.).
  $body_classes = array();
  // Random body class
  $random = theme_get_setting('layoutstudio_random_class');
  if($random > 1){
    $body_classes[] = 'random-'.rand(1, $random);
  }
  // Add a class that tells us whether we're on the front page or not.
  $body_classes[] = $variables['is_front'] ? 'front' : 'not-front';
  // Add a class that tells us whether the page is viewed by an authenticated user or not.
  $body_classes[] = $variables['logged_in'] ? 'logged-in' : 'not-logged-in';
  // Add arg(0) to make it possible to theme the page depending on the current page
  // type (e.g. node, admin, user, etc.). To avoid illegal characters in the class,
  // we're removing everything disallowed. We are not using 'a-z' as that might leave
  // in certain international characters (e.g. German umlauts).
  $body_classes[] = preg_replace('![^abcdefghijklmnopqrstuvwxyz0-9-_]+!s', '', 'page-'. form_clean_id(drupal_strtolower(arg(0))));
  // If on an individual node page, add the node type.
  if (isset($variables['node']) && $variables['node']->type) {
    $body_classes[] = 'node-type-'. form_clean_id($variables['node']->type);
    if(is_numeric(arg(1))){
    	$body_classes[] = 'node-'. arg(1);
    }
  }
  // Current active menu item
 	$body_classes[] = preg_replace('![^abcdefghijklmnopqrstuvwxyz0-9-_]+!s', '', 'menu-item-'. drupal_strtolower(menu_get_active_title()));
  // Add information regarding the secondary & tertiary.
  if ($variables['layout'] == 'regions-all') {
    $body_classes[] = 'regions-all';
  }
  elseif ($variables['layout'] == 'no-secondary-tertiary') {
    $body_classes[] = 'no-secondary-tertiary';
  }
  else {
    $body_classes[] = $variables['layout'];
  }
  $body_classes[] = $variables['preface_class'];

  if (!$variables['is_front']) {
		// Add unique classes for each page and website section
		$path = drupal_get_path_alias($_GET['q']);
		list($section, $sub_section) = explode('/', $path, 3);
		$body_classes[] = 'section-'. $section;
		$body_classes[] = 'sub-section-'. ($sub_section ? $sub_section : 'none');
		if (arg(0) == 'node') {
			if (arg(1) == 'add') {
				if ($section == 'node') {
					array_pop($body_classes); // Remove 'section-node'
					array_pop($body_classes); // Remove 'section-node'
				}
				$body_classes[] = 'section-node-add'; // Add 'section-node-add'
			}
			elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
				if ($section == 'node') {
					array_pop($body_classes); // Remove 'section-node'
					array_pop($body_classes); // Remove 'section-node'
				}
				$body_classes[] = 'section-node-'. arg(2); // Add 'section-node-edit' or 'section-node-delete'
			}
		}
	}
  // Implode with spaces.
  $variables['body_classes'] = implode(' ', $body_classes);
  
  // Create a copyright variable for use in the theme
  $copyright_start_year = theme_get_setting('copyright_start_year');
  $copyright_name       = theme_get_setting('copyright_name');
  $variables['copyright'] = copyright($copyright_start_year, $copyright_name);
  
  // Set the Credit text
  $credit_name = theme_get_setting('credit_name');
  $credit_options = theme_get_setting('credit_options');
  $credit_logo = theme_get_setting('credit_logo');
  $credit_destination = theme_get_setting('credit_destination');
  $variables['credit'] = credit($variables, $credit_name, $credit_options, $credit_logo, $credit_destination);
}

//Setting classes for nodes
function layoutstudio_preprocess_node(&$variables) {
	// Compile a list of classes that are going to be applied to the node div.
  $node_classes = array();
  if ($variables['sticky']) {
		$node_classes[] = 'sticky';
	}
	if ($variables['promote']) {
		$node_classes[] = 'promoted';
	}
	if (!$variables['node']->status) {
		$node_classes[] = 'node-unpublished';
		$variables['unpublished'] = TRUE;
	}
	else {
		$variables['unpublished'] = FALSE;
	}
	if ($variables['node']->uid && $variables['node']->uid == $user->uid) {
		// Node is authored by current user
		$node_classes[] = 'user-me';
	}
	//Add node's user id
	$node_classes[] = 'user-' . $variables['node']->uid;
	
	if ($variables['teaser']) {
		// Node is displayed as teaser
		$node_classes[] = 'node-teaser';
	}
	else {
		$node_classes[] = 'node-full';
	}
	//odd/even class for node listings
	$node_classes[] = $variables['zebra'];
	//node count for node listings
	$node_classes[] = 'count-' . $variables['id'];
	// Class for node type: "node-type-page", "node-type-story", "node-type-my-custom-type", etc.
	$node_classes[] = 'node node-type-'. $variables['node']->type;
	$variables['node_classes'] = implode(' ', $node_classes); // Concatenate with spaces  
}

//Setting classes for blocks
function layoutstudio_preprocess_block(&$variables) {
	$block_classes = array();
  // Random block class
  $random = theme_get_setting('layoutstudio_random_class');
  if($random > 1){
    $block_classes[] = 'random-'.rand(1, $random);
  }
  $block_classes[] = 'type block block-'. $variables['block']->module;
  $block_classes[] = $variables['block_zebra'];
  $block_classes[] = 'region-count-'. $variables['block_id'];
  $variables['block_classes'] = implode(' ', $block_classes);
}

//Setting classes for comments
function layoutstudio_preprocess_comment(&$variables) {
  static $comment_counter = array();
  
  if(!isset($comment_counter[$variables['node']->nid])) {
    $comment_counter[$variables['node']->nid] = 1; 
  }
  $comment_classes = array();
  $comment_classes[] = ($comment_counter[$variables['node']->nid] % 2) ? 'odd' : 'even';
  $comment_classes[] = 'comment-'.$variables['comment']->cid;
  $variables['comment_classes'] = implode(' ', $comment_classes);
  $comment_counter[$variables['node']->nid]++; 
}

/**
* @desc return formatted copyright information
*
* @param string $starting_year
*   must be 4 characters
*
* @param string $name
*   alternative 'name' for the sites copyright holder
*/                   
function copyright($starting_year = NULL, $name = NULL) {
  $site_name = ($name)? $name : variable_get('site_name', NULL);
  $current_year = date('Y'); 
  if ($starting_year && strlen($starting_year) == 4 && $starting_year != $current_year){
    $date = $starting_year .'&ndash;'. $current_year;
  }
  else{
    $date = $current_year;
  }     
  return '<span class="copyright">Copyright &copy; ' .$date. ' ' .$site_name. '. All rights reserved.</span> ';
}

/**
* @desc return formatted credit information
*/
function credit(&$variables, $credit_name, $credit_options, $credit_logo, $credit_destination) {
  global $theme_key;
  
  $output = '';
  
  if($credit_options == 'credit_all' || ($credit_options == 'credit_home' && $variables['is_front'])) {
	  
    $logo = '';
    if(!empty($credit_logo)) {
      $credit_class = 'credit-logo';
      $logo = ' style="background: url('.base_path().drupal_get_path("theme", $theme_key).'/'.$credit_logo.') no-repeat"';
    } 
    else {
    	$credit_class = 'credit-text';
    }
    $output .= '<span class="'.$credit_class.'">';
	  $output .= '<a title="' . $credit_name . '" href="' . $credit_destination . '"' . $logo . '>' . $credit_name . '</a>';
	  $output .= '</span>';
  }
	
	return $output;
}
