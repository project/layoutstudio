<?php

// Settings for the Layoutstudio theme

/**
* Implementation of THEMEHOOK_settings() function.
*
* @param $saved_settings
*   array An array of saved settings for this theme.
* @return
*   array A form array.
*/
function layoutstudio_settings($saved_settings) {

	// Add the form's CSS
  drupal_add_css(drupal_get_path('theme', 'layoutstudio') . '/theme-settings.css', 'theme');

  /*
   * The default values for the theme variables.
   */
  $defaults = layoutstudio_theme_get_default_settings('layoutstudio');

  // Merge the saved variables and their default values
  $settings = array_merge($defaults, $saved_settings);

  // Create the form widgets using Forms API
  $form['layoutstudio_random_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Body and Block class from random-1 to n'),
    '#default_value' => $settings['layoutstudio_random_class'],
    '#description' => t('This random number will appear as a body/block class that can be used to make a site more dynamic with CSS. For example, can be useful for creating header with rotating images on refresh. <strong>Value needs to be greater than one.</strong>'),
    '#weight' => '-1',
  );
  
  $form['layoutstudio_credit'] = array(
    '#type' => 'fieldset',
    '#title' => t('Credit Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('If you are creating a site for a client, this section allows you to put a credit logo (or text link) in the footer back to your company website.'),
  );
  
  $form['layoutstudio_credit']['credit_name'] = array(
    '#title' => t('Link title attribute of the credit logo'),
    '#type' => 'textfield',
    '#title' => t('Title tag of Layoutstudio.com credit logo'),
    '#default_value' => $settings['credit_name'],
  );
  
  $form['layoutstudio_credit']['credit_options'] = array(
    '#type' => 'radios',              
    '#title' => t('On what pages do you want the credit text or logo to appear.'),
    '#options' => array(
      "credit_home" => t('Home page only'),
      "credit_all" => t('All pages'),
      "credit_none" => t('None'),
    ),
    '#default_value' => $settings['credit_options'],
    '#attributes' => array('class' => 'credit'),
  );
  
  $form['layoutstudio_credit']['credit_logo'] = array(
    '#type' => 'textfield',
    '#title' => t('Credit Logo'),
    '#default_value' => $settings['credit_logo'],
    '#description' => t('Upload a credit logo in your <strong>' . base_path() . path_to_theme() . '</strong> directory. If left blank it will create a simple text link.'),
  );
  
  $form['layoutstudio_credit']['credit_destination'] = array(
    '#type' => 'textfield',
    '#title' => t('Credit Link Destination'),
    '#default_value' => $settings['credit_destination'],
  );
  
  $form['layoutstudio_copyright'] = array(
    '#type' => 'fieldset',
    '#title' => t('Copyright Information'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['layoutstudio_copyright']['copyright_start_year'] = array(
    '#type' => 'textfield',
    '#title' => t('Year to show as start of copyright range'),
    '#default_value' => $settings['copyright_start_year'],
    '#description' => t('This will show yyyy-xxxx for multiple years and xxxx for single years, where yyyy is the text entered, and xxxx is the same as the current year.'),
  );
  
  $form['layoutstudio_copyright']['copyright_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name to show as copyright holder'),
    '#default_value' => $settings['copyright_name'],
    '#description' => t('This is the text that will appear after the date. Allows a different name than the site name.'),
  );
  
  $form['layoutstudio_colors'] = array(
    '#type' => 'fieldset',
    '#title' => t('Region Colors'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('This will add background colors to each region to better illustrate the position of each region in the layout. Useful for testing and getting familiar with the theme. Here is a  legend for each region\'s color:
    		<ul class="color-reference regions">
    			<li class="region-header">Header</li>
    			<li class="region-preface">Preface</li>
    			<li class="region-primary">Primary</li>
    			<li class="region-secondary">Secondary</li>
    			<li class="region-tertiary">Tertiary</li>
    			<li class="region-postscript">Postscript</li>
    			<li class="region-footer">Footer</li>
    		</ul class="color-reference">'),
    
  );
  
  $form['layoutstudio_colors']['color_options'] = array(
    '#type' => 'radios',
    '#title' => t('Show colors for Regions'),
    '#options' => array(
      1 => t('Yes'),
      0 => t('No'),
    ),
    '#default_value' => $settings['color_options'],
    '#attributes' => array('class' => 'colors'),
  );
  
  // Return the additional form widgets
  return $form;
}

/**
 * Return the theme settings' default values from the .info and save them into the database.
 *
 * @param $theme
 *   The name of theme.
 */
function layoutstudio_theme_get_default_settings($theme) {
  $themes = list_themes();

  // Get the default values from the .info file.
  $defaults = !empty($themes[$theme]->info['settings']) ? $themes[$theme]->info['settings'] : array();

  if (!empty($defaults)) {
    // Get the theme settings saved in the database.
    $settings = theme_get_settings($theme);
    // Don't save the toggle_node_info_ variables.
    if (module_exists('node')) {
      foreach (node_get_types() as $type => $name) {
        unset($settings['toggle_node_info_' . $type]);
      }
    }
    // Save default theme settings.
    variable_set(
      str_replace('/', '_', 'theme_' . $theme . '_settings'),
      array_merge($defaults, $settings)
    );
    // If the active theme has been loaded, force refresh of Drupal internals.
    if (!empty($GLOBALS['theme_key'])) {
      theme_get_setting('', TRUE);
    }
  }

  // Return the default settings.
  return $defaults;
}